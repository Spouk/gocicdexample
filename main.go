package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
)

type HelloUser struct {
	stock []User
}

//can use in ext api
func (h *HelloUser) Send(msg string) error {
	if len(h.stock) == 0 {
		return errors.New("warning: count stock users eq 0")
	}
	for _, j := range h.stock {
		j.SendMsg(msg)
	}
	log.Printf("sended msg: %d\n", len(h.stock))
	return nil
}
func (h *HelloUser) Add(u ...User) {
	h.stock = append(h.stock, u...)
}

//can use in ext api
func (h *HelloUser) GetStock() []string {
	var res []string
	for _, x := range h.stock {
		s, err := x.ToJson()
		if err != nil {
			continue
		}
		res = append(res, s)
	}
	return res
}

type User struct {
	Name, Password, Email string
	channel               io.Writer
}

func (m *User) SendMsg(msg string) {
	fmt.Fprintf(m.channel, fmt.Sprintf("[%#v] `%v'\n", m, msg))
}
func (m User) String() string {
	return fmt.Sprintf("%#v", m)
}
func (m *User) ToJson() (string, error) {
	res, err := json.Marshal(m)
	if err != nil {
		return "", err
	}
	return string(res), nil
}
func main() {
	h := HelloUser{
		stock: []User{},
	}
	h.Add(
		User{
			Name:     "TesterName",
			Password: "TesterPassword",
			Email:    "test@test.com",
			channel:  os.Stdout,
		},
		User{
			Name:     "TesterName2",
			Password: "TesterPassword2",
			Email:    "test2@test.com",
			channel:  os.Stdout,
		})
	var msg = "test message"
	if err := h.Send(msg); err != nil {
		fmt.Println(err)
	}
	fmt.Println(h.GetStock())
}
