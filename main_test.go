package main

import (
	"bytes"
	"fmt"
	"os"
	"strings"
	"testing"
)

func TestSend(t *testing.T) {
	h := HelloUser{
		stock: []User{},
	}
	h.Add(
		User{
			Name:     "TesterName",
			Password: "TesterPassword",
			Email:    "test@test.com",
			channel:  os.Stdout,
		},
		User{
			Name:     "TesterName2",
			Password: "TesterPassword2",
			Email:    "test2@test.com",
			channel:  os.Stdout,
		})
	var msg = "test message"
	if err := h.Send(msg); err != nil {
		t.Errorf("error %v\n", err)
	}
}
func TestUser_String(t *testing.T) {
	u := User{
		Name:     "TesterName",
		Password: "TesterPassword",
		Email:    "test@test.com",
		channel:  os.Stdout,
	}
	res := fmt.Sprintf("%#v", u)
	if strings.Compare(res, u.String()) != 0 {
		t.Errorf("%v != %v", res, u.String())
	}
}
func TestUser_SendMsg(t *testing.T) {
	buf := bytes.NewBuffer([]byte{})
	u := &User{
		Name:     "TesterName",
		Password: "TesterPassword",
		Email:    "test@test.com",
		channel:  buf,
	}
	var msg = "test message"
	u.SendMsg(msg)
	res := fmt.Sprintf("[%#v] `%v'\n", u, msg)
	if strings.Compare(buf.String(), res) != 0 {
		t.Errorf("%v != %v", buf.String(), res)
	}
}
func TestUser_ToJson(t *testing.T) {
	u := User{
		Name:     "TesterName",
		Password: "TesterPassword",
		Email:    "test@test.com",
		channel:  os.Stdout,
	}
	_, err := u.ToJson()
	if err != nil {
		t.Errorf("error converting: %v", err)
	}
}
